# Makefile for Data Structures Binary Search Tree Assignments

SRCDIR = src
BINDIR = bin
LIB= Lib
CODEC = $(LIB)/commons-codec-1.7.jar

JAVAC = javac
JFLAGS =-g -d $(BINDIR) -cp $(BINDIR):$(CODEC)
JAVA= -cp $(CODEC)


vpath %.java $(SRCDIR):$(SRCDIR)/thread:$(SRCDIR)/model:$(SRCDIR)/map:$(SRCDIR)/driver
vpath %.class $(BINDIR)

#/byte_array_size:$(BINDIR)/thread:$(BINDIR)/overidden:$(BINDIR)/map:$(BINDIR)/driver

# define general build rule for java sources
.SUFFIXES:  .java  .class

.java.class:
	$(JAVAC)  $(JFLAGS)  $<

#default rule - will be invoked by make


all: AsymmetricCryptography.class GenerateKeys.class Message.class VerifyMessage.class ClientInformation.class MessageReaderFromServer.class\
	MessageSenderToServer.class SocketUserMap.class ServerThread.class Server.class Client.class ClientMessageListener.class  

rebuild:
	make clean
	make all


# The following two targets deal with the mutual dependency between Position and BinaryTree
ServerThread.class: SocketUserMap.class
# Server.class: ServerThread.class
# SocketUserMap.class: ClientMessageListener.class
# ClientMessageListener.class: SocketUserMap.class

# BinaryTree.class:
#	rm -rf $(BINDIR)/binarytree/Position.class $(BINDIR)/binarytree/BinaryTree.class
#	javac $(JFLAGS) $(SRCDIR)/binarytree/Position.java $(SRCDIR)/binarytree/BinaryTree.java

SocketUserMap.class:
	rm -rf $(BINDIR)/thread/ClientMessageListener.class $(BINDIR)/model/SocketUserMap.class
	javac $(JFLAGS) $(SRCDIR)/model/SocketUserMap.java $(SRCDIR)/thread/ClientMessageListener.java $(SRCDIR)/thread/ServerThread.java $(SRCDIR)/driver/Server.java

# ClientMessageListener.class:
#	rm -rf $(BINDIR)/thread/ClientMessageListener.class $(BINDIR)/map/SocketUserMap.class
#	javac $(JFLAGS) $(SRCDIR)/thread/ClientMessageListener.class $(SRCDIR)/map/SocketUserMap.class
server:
	java -cp $(BINDIR):$(CODEC) driver/Server

client:
	java -cp $(BINDIR):$(CODEC) driver/Client

clean:
	@rm -f  $(BINDIR)/*.class
	@rm -f $(BINDIR)/*/*.class
