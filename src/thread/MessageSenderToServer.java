package thread;

import driver.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Arjun on 4/5/2017.
 */
public class MessageSenderToServer extends Thread {
    private PrintWriter p;
    private MessageReaderFromServer r;
    private BufferedReader bufferedReader;
    private Socket socket;
    private volatile boolean interrupted = false;
    private String username;
    private PublicKey publicKey;
    private String publicKeyFilename;
    private AsymmetricCryptography ac;
    private PrivateKey privateKey;
    private String privateKeyFileName;
    private static AtomicLong numberGenerator;
    private String signedFilePath;

    public MessageSenderToServer(Socket socket, String username, PublicKey publicKey, String filename, PrivateKey privateKey, AsymmetricCryptography ac,String privateKeyFileName, MessageReaderFromServer r){ // Reads message from client and sends it to server
        this.socket = socket;
        this.username = username;
        this.publicKey = publicKey;
        this.publicKeyFilename = filename;
        this.privateKey = privateKey;
        this.ac = ac;
        this.privateKeyFileName=privateKeyFileName;
        this.numberGenerator = new AtomicLong(910000000000L);
        this.signedFilePath = "MyData/"+username+"/Message.txt";
        this.r = r;
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            p = new PrintWriter(socket.getOutputStream());
            p.println(this.username);
            p.flush();
            p = new PrintWriter(socket.getOutputStream());

        } catch (Exception e){
            System.out.println("Error in MessageSenderToServer");
        }
    }

    private static long GenerateSequenceNumber(){
        return numberGenerator.getAndIncrement();
    }

    private static Timestamp GenerateTimeStamp(){
        return new Timestamp(System.currentTimeMillis());
    }

    private static String AddTimeStampAndSequenceNumber(String message){
        return GenerateTimeStamp() +","+GenerateSequenceNumber()+","+ message;
    }

    private static void SendSignedMessage(String path, Socket socket) throws IOException {
        InputStream in = new FileInputStream(new File(path));
        OutputStream out = socket.getOutputStream();
        byte[] bytes = new byte[1024];

        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
            if (count < 1024){
                break;
            }
        }
        System.out.println("Sent the digital signature with the message");
    }

    private static void SendPublicKey(String publicKeyFilename, Socket socket) throws IOException {
        InputStream in = new FileInputStream(new File(publicKeyFilename));
        OutputStream out = socket.getOutputStream();
        byte[] bytes = new byte[1024];
        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
        }
    }

    private static void SignEncryptedMessage(String privateKeyFileName, String message, String path) throws Exception {
        new Message(message, privateKeyFileName).writeToFile(path);
    }

    private static String EncryptMessage(String message, PublicKey publicKey, AsymmetricCryptography ac) throws NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidKeyException {
        return ac.encryptText(AddTimeStampAndSequenceNumber(message), publicKey);
    }

    private PublicKey getOtherUserPublicKey(AsymmetricCryptography ac) throws Exception {
        return ac.getPublic(r.getPublicKeyPath());
    }
    public void run(){

        while(!interrupted && !socket.isClosed()) {
            try {
                String message = bufferedReader.readLine(); // standard input
                if(message.trim().equalsIgnoreCase("@exit")){ // Exiting the chat
                    this.interrupt();
                }
                else if (message.trim().equalsIgnoreCase("@help")){
                    String help = "1) Type @connectto <Username> to connect to a user."+
                    "\n2) To exit the chat, type '@exit'"
                    ;
                    System.out.println(help);
                }
                else {
                    if(!message.trim().equalsIgnoreCase("")) {
                        p = new PrintWriter(socket.getOutputStream());

                        // send the public key to user you connecting to
                        if (message.trim().contains("@connectto ")) {
                            p.println(message);
                            p.flush();

                            SendPublicKey(publicKeyFilename, socket);
                            System.out.println("(connected)");

                        }
                        else if(message.charAt(0) == '@'){
                            p.println(message);
                            p.flush();
                            System.out.println("(Message Sent)");
                        }
                        else {  // sending the message to the server
                            p.println("@receiveMessage");
                            p.flush();
                            PublicKey publicKey = getOtherUserPublicKey(ac);
                            String encryptedMessage = EncryptMessage(message, publicKey, ac);
                            SignEncryptedMessage(privateKeyFileName, encryptedMessage, signedFilePath);
                            SendSignedMessage(signedFilePath,socket); //send the file
                        }
                    }
                }
            }
            catch (java.net.SocketException e1){
                this.interrupt();
                // e1.printStackTrace();

            }
            catch (Exception e2) {
                // e2.printStackTrace();
            }
        }
    }

    public void interrupt(){
        this.interrupted = true;
        try{
            if(bufferedReader != null) {
                bufferedReader.close();
            }

            if(p!= null){
                p.close();
            }

            socket.close();
        } catch (Exception e){
        }
    }
}
