package thread;

import model.SocketUserMap;

import java.io.*;
import java.util.Arrays;

public class ClientMessageListener extends Thread {

    private SocketUserMap map;
    private ServerThread server;

    public ClientMessageListener(SocketUserMap map, ServerThread server){
        this.server = server;
        this.map = map;
    }

    public void run(){
        try{

            while(true){
                System.out.println("Getting message");
                String message = map.getReader().readLine();
                System.out.println(map.getUserId() + ": " + message);
                if(message.trim().contains("@connectto ")){
                    System.out.println(message);
                    String connectingTo = message.substring( 11, message.length());
                    if(!connectingTo.trim().equalsIgnoreCase(map.getUserId())) {
                        for (SocketUserMap user : server.getMaps()) {
                            if (user.getUserId().equalsIgnoreCase(connectingTo.trim())) {
                                map.setConnectedTo(user);
                                user.setConnectedTo(map);

                                OutputStream out = map.getConnectedTo().getSocket().getOutputStream();
                                System.out.println("Printing to user " + map.getConnectedTo().getUserId());
                                PrintWriter printWriter = new PrintWriter(out);
                                printWriter.write(map.getUserId() + " says: " + message + "\n");
                                printWriter.flush();

                                File test = new File("Test");
                                FileOutputStream fos = new FileOutputStream(test);
                                System.out.println("Server is sending PK");
                                printWriter.write("@ReceivePK: " + map.getUserId() + "\n");
                                printWriter.flush();

                                // sending the public key file to the recipient
                                InputStream in = map.getSocket().getInputStream();
                                byte[] bytes = new byte[1024];
                                int count;
                                while ((count = in.read(bytes)) > 0) {
                                    fos.write(bytes, 0, count);
                                    if (count < 1024){
                                        break;
                                    }

                                }
                                fos.close();

                                File test1 = new File("Test");
                                FileInputStream fin = new FileInputStream(test1);
                                bytes = new byte[1024];
                                while ((count = fin.read(bytes)) > 0) {
                                    out.write(bytes, 0, count);
                                    if (count < 1024){
                                        break;
                                    }
                                }
                                fin.close();
                            }
                        }
                    }
                } if(message.trim().equalsIgnoreCase("@receiveSecondPK")){
                    InputStream in = map.getSocket().getInputStream();
                    File file = new File("Test");
                    FileOutputStream fos = new FileOutputStream(file);
                    byte[] bytes = new byte[1024];
                    int count;
                    while ((count = in.read(bytes)) > 0) {
                        fos.write(bytes, 0, count);
                        if (count < 1024){
                            break;
                        }
                    }
                    fos.close();
                    System.out.println("Public key of " + map.getUserId() + " has been received on the server and " +
                            "needs to be sent to user " + map.getConnectedTo().getUserId());

                    OutputStream out = map.getConnectedTo().getSocket().getOutputStream();
                    PrintWriter printWriter = new PrintWriter(out);
                    printWriter.write("@ServerSendingSecondPK: " + map.getUserId() + "\n");
                    printWriter.flush();
                    printWriter.flush();

                    System.out.println("Server is sending the PK of " + map.getUserId() + " to " + map.getConnectedTo().getUserId());

//                     printWriter.close();

                    file = new File("Test");
                    FileInputStream fin = new FileInputStream(file);
                    bytes = new byte[1024];
                    while ((count = fin.read(bytes)) > 0) {
                        map.getConnectedTo().getSocket().getOutputStream().write(bytes, 0, count);
                        if (count < 1024){
                            break;
                        }
                    }
                    fin.close();

                } else if(message.trim().equalsIgnoreCase("@users")){
                    String allUsers = "[ ";
                    for(SocketUserMap user : server.getMaps()){
                        allUsers += user.getUserId() + ", ";
                    }
                    if(allUsers.substring(allUsers.length()-2, allUsers.length()).equalsIgnoreCase(", ")){
                        allUsers = allUsers.substring(0, allUsers.length() - 2);
                    }
                    allUsers += " ]";
                    OutputStream out = map.getSocket().getOutputStream();
                    PrintWriter printWriter = new PrintWriter(out);
                    printWriter.write("List of Users: " + allUsers + "\n");
                    printWriter.flush();
                } else {
                    if(message.trim().contains("@receiveMessage")) {

                        OutputStream out = map.getConnectedTo().getSocket().getOutputStream();
                          // start reading in the file and forward to the receipient

                            File test = new File("Message.txt");
                            FileOutputStream fos = new FileOutputStream(test);

                            // receive the file
                            InputStream in = map.getSocket().getInputStream();
                            byte[] bytes = new byte[1024];
                            int count;
                            while ((count = in.read(bytes)) > 0) {
                                fos.write(bytes, 0, count);
                                if (count < 1024){
                                    break;
                                }
                            }
                            fos.close();
//                             //send the file to the recipient
                            System.out.println("saved message ");
                            // send the message to the client

                        PrintWriter p = new PrintWriter(out);
                        p.println("@Message");
                        p.flush();

                        FileInputStream fin = new FileInputStream(test);
                        while((count = fin.read(bytes)) > 0){
                            out.write(bytes, 0, count);
                            if(count < 1024){
                                break;
                            }
                        }
                        out.flush();
                        fin.close();
                    }
                }
            }

        }
        catch (Exception e){

        }
    }
}
