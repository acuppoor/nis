package model;

import thread.ClientMessageListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketUserMap {

    private String userId;
    private Socket socket;
    private BufferedReader reader;
    private ClientMessageListener listener;
    private SocketUserMap connectedTo;

    public SocketUserMap(Socket socket, BufferedReader reader){
        this.socket = socket;
        this.reader = reader;
        try {
            this.userId = reader.readLine().trim();
            System.out.println("New User: " + userId);
        } catch (Exception e){

        }
    }

    public ClientMessageListener getListener() {
        return listener;
    }

    public void setListener(ClientMessageListener listener) {
        this.listener = listener;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public BufferedReader getReader() {

        try{
            return new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e){
            return reader;
        }
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }

    public SocketUserMap getConnectedTo() {
        return connectedTo;
    }

    public void setConnectedTo(SocketUserMap connectedTo) {
        this.connectedTo = connectedTo;
    }

    public void dispose(){
        listener.interrupt();
        try {
            reader.close();
            socket.close();
        } catch (Exception e){

        }
    }
}
