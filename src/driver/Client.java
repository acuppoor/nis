package driver;

import thread.MessageReaderFromServer;
import thread.MessageSenderToServer;

import java.io.*;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;


/**
 * Created by Arjun on 4/5/2017.
 */
public class Client {
    public static String IPAddress = "localhost";
    private static String username;

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in); // to get port number and messages from user
        BufferedReader reader;
        File file;
        String key = "";
        PrivateKey privateKey=null;
        PublicKey publicKey=null;
        String publicKeyFile;
        String privateKeyFile ;
        AsymmetricCryptography ac = null;

        try{
            System.out.print("Enter an IP Address of server (press enter to use localhost):\n");
            String ip = scanner.next().trim().toLowerCase();
            if(!ip.equalsIgnoreCase("")){
                IPAddress = ip;
            }

            System.out.println("Enter a username:");
            username = scanner.next().trim().toLowerCase();

        } catch (Exception e){
            IPAddress = "localhost";
        }
        System.out.println("IP Address: " + IPAddress);

        System.out.println("Connecting to server...");

        publicKeyFile = "KeyPair/"+username+"/publicKey";
        privateKeyFile = "KeyPair/"+username+"/privateKey";

        GenerateKeys gk;
        try {
            gk = new GenerateKeys(1024);
            gk.createKeys();
            gk.writeToFile(publicKeyFile, gk.getPublicKey().getEncoded());
            gk.writeToFile(privateKeyFile, gk.getPrivateKey().getEncoded());
            ac = new AsymmetricCryptography();
            privateKey = ac.getPrivate(privateKeyFile);
            publicKey = ac.getPublic(publicKeyFile);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }


        Socket socket = null;
        boolean connected = false;
        int connectionTrialLimit = 0;
        while(!connected){


            try{
                socket = new Socket(IPAddress, Server.SERVER_PORT);
                connected = true;

                System.out.println("Type '@help' for help messages");
                System.out.println("----- Chat Begin -----");

            } catch (Exception e){
                // Server is offline
                connectionTrialLimit++;
                if(connectionTrialLimit > 1000){
                    System.out.println("Server not online yet. Please try again later.");
                    System.exit(0);
                }
            }
        }

        try {
            MessageReaderFromServer r = new MessageReaderFromServer(socket, privateKey, username);
            MessageSenderToServer s = new MessageSenderToServer(socket, username, publicKey, publicKeyFile, privateKey,ac,  privateKeyFile, r);
            r.start();
            s.start();
            r.join();
            s.join();
            System.out.println("----- Chat End -----");
        } catch (Exception e){
        }
    }
}
