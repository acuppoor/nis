package driver;

import thread.ServerThread;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;

public class Server {

    public static int SERVER_PORT = 6781;
    private static String serverIp;
    private static ServerSocket serverSocket;

    public static void main(String[] args) throws IOException{ //Exceptions thrown for method
        System.out.println("----- Server Started -----");
        try {
            InetAddress iAddress = InetAddress.getLocalHost();
            serverIp = iAddress.getHostAddress();
            System.out.println("Server IP address : " + serverIp);
            serverSocket = new ServerSocket(SERVER_PORT);
            ServerThread st = new ServerThread(serverSocket);
            st.start();
            st.join();
            System.out.println("----- Server Ended -----");
        } catch (Exception e) {
            System.out.println("Error in Server Main");
        }
    }

}
